package com.quick.sqlitequick;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {
    dbHelper helper;
    String mQuery;
    Button submit, daftar;
    EditText et_noInduk, et_nama, et_alamat, et_noTelp;
    RadioButton rb_l, rb_p;
    String no_induk, nama, jk, alamat, no_telp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        allowNetworkOnMainThread();

        et_noInduk = findViewById(R.id.et_induk);
        et_nama = findViewById(R.id.et_nama);
        et_noTelp = findViewById(R.id.et_telp);
        et_alamat = findViewById(R.id.et_alamat);
        rb_l = findViewById(R.id.rb_p);
        rb_p = findViewById(R.id.rb_w);
        submit = findViewById(R.id.bt_submit);
        daftar = findViewById(R.id.bt_list);

        helper = new dbHelper(this);
        Stetho.initializeWithDefaults(this);

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, ListActivity.class);
                startActivity(in);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                no_induk = et_noInduk.getText().toString();
                nama = et_nama.getText().toString();
                no_telp = et_noTelp.getText().toString();
                alamat = et_alamat.getText().toString();

                if (rb_l.isChecked() ){
                    jk = "L";

                } else if (rb_p.isChecked()){
                    jk = "P";
                }

                if (no_induk != null && nama != null && alamat != null
                        && jk != null && no_telp != null){
                    insertDataSQLite();
                } else {
                    Toast.makeText(getApplicationContext(), "Data anda belum lengkap", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    void allowNetworkOnMainThread() {
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//    }

    void insertDataSQLite(){
        SQLiteDatabase db = helper.getWritableDatabase();
        mQuery = "INSERT INTO tabel_pribadi \n" +
                "VALUES(null, '"+no_induk+"','"+nama+"','"+jk+"','"+alamat+"','"+no_telp+"')";
        Log.d("Query", mQuery);
        db.execSQL(mQuery);
        Toast.makeText(this, "Insert Success", Toast.LENGTH_SHORT).show();
    }

    void update(){
        nama = et_nama.getText().toString();
        no_telp = et_noTelp.getText().toString();
        alamat = et_alamat.getText().toString();

        if (rb_l.isChecked() ){
            jk = "L";

        } else if (rb_p.isChecked()){
            jk = "P";
        }
        SQLiteDatabase db = helper.getWritableDatabase();
        mQuery = "UPDATE tabel_pribadi \n" +
                "SET nama_lengkap = '"+nama+"', \n" +
                "jenis_kelamin = '"+jk+"', \n" +
                "alamat = '"+alamat+"', \n" +
                "no_telp = '"+no_telp+"' \n" +
                "WHERE no_induk = '"+no_induk+"'";
        Log.d("Query", mQuery);
        db.execSQL(mQuery);
    }
}
