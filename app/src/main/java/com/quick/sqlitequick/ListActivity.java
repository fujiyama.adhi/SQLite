package com.quick.sqlitequick;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    ListView lv_list;
    dbHelper helper;
    String mQuery, no_induk;
    ArrayList<String> list_no;
    ArrayList<String> list_nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        helper = new dbHelper(this);


        lv_list = findViewById(R.id.lv_list);
        showData();

        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in = new Intent(ListActivity.this, MainActivity.class);
                in.putExtra("no_induk", list_no.get(i));
                startActivity(in);
            }
        });

        lv_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                no_induk = list_no.get(i);
                deleteData();
                return false;
            }
        });
    }

    void showData(){
        list_no = new ArrayList<>();
        list_nama = new ArrayList<>();
        SQLiteDatabase db = helper.getWritableDatabase();
        mQuery = "SELECT no_induk, nama_lengkap FROM tabel_pribadi";
        Cursor cursor = db.rawQuery(mQuery, null);
        while (cursor.moveToNext()){
            list_no.add(cursor.getString(0));
            list_nama.add(cursor.getString(1));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list_nama);
        lv_list.setAdapter(adapter);
    }

    void deleteData(){
        SQLiteDatabase db = helper.getWritableDatabase();
        mQuery = "DELETE FROM tabel_pribadi WHERE NO_INDUK = '"+no_induk+"'";
        Log.d("Query", mQuery);
        db.execSQL(mQuery);

        Toast.makeText(this, "Data Terhapus", Toast.LENGTH_SHORT).show();
        showData();
    }
}
