package com.quick.sqlitequick;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by trial on 1/15/18.
 */

public class dbHelper extends SQLiteOpenHelper {
    public dbHelper(Context context) {
        super(context, "db_training", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String mQuery = "CREATE TABLE tabel_pribadi (" +
                "id INTEGER PRIMARY KEY,"+
                "no_induk TEXT,"+
                "nama_lengkap TEXT,"+
                "jenis_kelamin TEXT,"+
                "alamat TEXT,"+
                "no_telp TEXT)";
        db.execSQL(mQuery);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
